export class TouristAttractionForm {
  constructor() {
    // this.list = [];
    this.list = [
      {
        title: "Pão de Açúcar",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "./images/pao-de-acucar.png",
      },
      {
        title: "Ilha Grande",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "./images/ilha-grande.png",
      },
      {
        title: "Cristo Redentor",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "./images/cristo-redentor.png",
      },
      {
        title: "Centro Histórico de Paraty",
        description:
          "Amet minim mollit non deserunt ullamco est sit aliqua dolor dosa amet sint. Velit officia consece duis enim velit mollit.",
        image: "./images/paraty.png",
      },
    ];
    this.selector();
    this.events();
    this.renderListAttractions();
  }

  selector() {
    this.inputFile = document.querySelector(".fake-input-file__image-input");
    this.imageFile = document.querySelector(".fake-input-file__image");
    this.placeholderFile = document.querySelector(".fake-input-file__placeholder");
    this.titleInput = document.querySelector(".inputs-text__title");
    this.descriptionInput = document.querySelector(".inputs-text__description");
    this.form = document.querySelector(".attraction-form");
    this.attractions = document.querySelector(".attractions-result__list");
  }

  events() {
    this.inputFile.addEventListener("change", this.readURLImage.bind(this));
    this.inputFile.addEventListener("ondrop", this.readURLImage.bind(this));
    this.form.addEventListener("submit", this.addItemToList.bind(this));
  }

  readURLImage() {
    if (this.inputFile.files.length <= 0) {
      return;
    }

    let reader = new FileReader();

    reader.onload = () => {
      this.imageFile.src = reader.result;
    };

    this.imageFile.style.display = "inline-block";

    this.placeholderFile.style.display = "none";

    reader.readAsDataURL(this.inputFile.files[0]);
  }

  addItemToList(event) {
    event.preventDefault();

    const pattern = /\/(jpe?g|png|gif)/;
    const attractionFile = event.target["attraction-image"].files[0].type;
    const attractionTitle = event.target["attraction-title"].value.trim();
    const attractionDescription = event.target["attraction-description"].value.trim();
    const attractionImage = this.imageFile.src;

    if (!attractionFile.match(pattern)) {
      alert("O arquivo deve ser do tipo .png, .jpg ou .gif");
      return;
    }

    if (attractionTitle === "" || attractionDescription === "" || attractionImage === "") {
      alert("Todos os campos devem ser preenchidos corretamente!");
      return;
    }

    const attraction = {
      title: attractionTitle,
      description: attractionDescription,
      image: attractionImage,
    };

    this.list.push(attraction);
    this.renderListAttractions();
    this.resetInputs();
  }

  renderListAttractions() {
    let attractionsStructure = "";
    this.list.forEach((attraction) => {
      attractionsStructure += `
      <li class="attractions-result__item">
      <div  class="attractions-result__image">
      <img src="${attraction.image}"></img>
      </div>
      <div class="attractions-result_text">
      <h3>${attraction.title}</h3>
      <div class="attractions-result__description">
      <p>${attraction.description}</p>
      </div>
      </div>
      </li>
      `;
    });

    this.attractions.innerHTML = attractionsStructure;
  }

  resetInputs() {
    this.descriptionInput.value = "";
    this.titleInput.value = "";
    this.inputFile.value = "";
    this.imageFile.src = "";
    this.imageFile.style.display = "none";
    this.placeholderFile.style.display = "block";
  }
}
