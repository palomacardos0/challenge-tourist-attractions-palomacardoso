import { TouristAttractionForm } from "./components/touristAttractionForm";

document.addEventListener("DOMContentLoaded", function () {
  new TouristAttractionForm();
});
